//
//  Created by jsn with love for you.
//  Copyright © 2019 hidesoft. All rights reserved.
//

import UIKit
import ObjectMapper

// MARK: - Cell Types
/*
struct Cell___VARIABLE_moduleName___Type {
    static let header = "Header"
    static let cell = "Cell"
}*/


class ___VARIABLE_moduleName___ViewModel : BaseViewModel {
    
    // MARK: - Router
    var router: ___VARIABLE_moduleName___Router!

    func loadData() {
        
    }

    func atras() {
        router.atras()
    }

    func continuar() {
        router.continuar()
    }
    

}
