//
//  BaseRouting.swift
//  arquitectura
//
//  Created by Jeisson Gonzalez on 8/28/19.
//  Copyright © 2019 hidesoft. All rights reserved.
//

import UIKit

class BaseRouting {
    private var _viewController : UIViewController!
    
    
    var view: UIViewController {
        get{
            return _viewController
        }
        set {
            _viewController = newValue
        }
        
    }
    
    func push(viewController:UIViewController){
      _viewController.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func present(viewController:UIViewController){
        
        viewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        viewController.providesPresentationContextTransitionStyle = true
        viewController.definesPresentationContext = true
        viewController.modalPresentationStyle = .overCurrentContext
        
        _viewController.present(viewController, animated: true, completion: nil)
    }
    
    class func start(withController viewController:UIViewController){
        DispatchQueue.main.async {
            
            let nav = UINib(nibName: "NavigationController", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UINavigationController
            nav.navigationBar.isOpaque = true
            nav.viewControllers = [viewController]
            
            
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.window?.rootViewController = nav
            
        }
    }
    
    class func start(withNavController nav:UINavigationController){
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.window?.rootViewController = nav
        }
    }
    
    class func instance<T : UIViewController>(_ viewControllerClass: T.Type) -> T {
        
        let storyboardID = "\(T.self)"
        let storyboard = UIStoryboard(name: storyboardID, bundle: nil)
        
        guard let vc = storyboard.instantiateViewController(withIdentifier: storyboardID) as? T else {
            fatalError("ViewController with identifier \(storyboardID), not found in \(storyboard) Storyboard")
        }
        
        return vc
        
    }
    
    
}
