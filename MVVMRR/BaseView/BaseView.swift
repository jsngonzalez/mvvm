//
//  BaseView.swift
//  pixo
//
//  Created by Jeisson Gonzalez on 9/6/19.
//  Copyright © 2019 hidesoft. All rights reserved.
//

import UIKit

class BaseView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let yourBackImage = UIImage(named: "flecha")
        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        self.navigationController?.navigationBar.backItem?.title = ""
        
        self.navigationItem.backBarButtonItem?.title = ""
        
        //self.navigationItem.setHidesBackButton(true, animated:false);
        
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        let logo = #imageLiteral(resourceName: "logo-palabras")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParent || isBeingDismissed {
            print("view controller eliminado")
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
}
