//
//  AppModel.swift
//  pixo
//
//  Created by MacBook on 9/5/19.
//  Copyright © 2019 hidesoft. All rights reserved.
//

import ObjectMapper

class AppModel: Mappable {
 
    var container_producto = [TableModel]()
    var archivos = [ArchivoModel]()
    var nuevo = ArchivoModel(JSON: [:])!
    var mostrarTerminosCondiciones = true
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        container_producto <- map["container_producto"]
        archivos <- map["productos"]
        nuevo <- map["nuevo"]
        mostrarTerminosCondiciones <- map["primaraCargaApp"]
    }
    
}
