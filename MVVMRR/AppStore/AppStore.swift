//
//  AppStore.swift
//  pixo
//
//  Created by Jeisson Gonzalez on 9/5/19.
//  Copyright © 2019 hidesoft. All rights reserved.
//

import UIKit
import SwiftyUserDefaults


extension DefaultsKeys {
    static let app = DefaultsKey<[String: Any]>("obj_app", defaultValue: [:])
    static let error = DefaultsKey<[String: Any]>("obj_error", defaultValue: [:])
}

extension Notification.Name {
    static let appStore_newState = Notification.Name("appStore_newStateappStore_newState")
}

typealias callBackNewStore = (()->())
typealias callBackNewState = ((StateModel)->())

class ObjObserver:NSObject {
    private var _id = ""
    private var _observer : NSObjectProtocol!
    private var _callBack : callBackNewStore!
    
    var observer : NSObjectProtocol {
        get{
            return _observer
        }
        set{
            _observer = newValue
        }
    }
    
    var ID : String {
        get{
            return _id
        }
        set{
            _id = newValue
        }
    }
    
    
    var callback : callBackNewStore {
        get{
            return _callBack
        }
        set{
            _callBack = newValue
        }
    }
    
    func  unsubscribe() {
        NotificationCenter.default.removeObserver((_observer as Any))
        _observer = nil
        _callBack = nil
    }
    
    deinit {
        unsubscribe()
    }
    
}



class AppStore {
    
    private var _observerArray = [ObjObserver]()
    private var _state : StateModel = StateModel.init(.none, mensaje: "")
    private var _callbackNewState : callBackNewState?
    
    var state : StateModel {
        get{
            return _state
        }
        set{
            if let callback = _callbackNewState {
                callback(newValue)
            }
        }
    }
    
    var store = ( Defaults.hasKey(.app) ? AppModel(JSON: Defaults[.app] )!  : AppModel(JSON: [:])! )
    
    func observer<T>(_ state:T, whenNewStore callback: @escaping callBackNewStore, whenNewState callNewState: @escaping callBackNewState ){
        
        _callbackNewState = callNewState
        
        let ID = "\(T.self)_" + String(format: "%p", unsafeBitCast(T.self, to: Int.self))
        let nuevo = ObjObserver()
        nuevo.ID = ID
        nuevo.callback = callback
        nuevo.observer = NotificationCenter.default.addObserver( forName: .appStore_newState, object: nil, queue: nil) { notification in
            
            //print("⚡️ \(ID) recibe notificacion")
            self.store =  AppModel(JSON: Defaults[.app] )!
            nuevo.callback()
        }
        
        self._observerArray.append(nuevo)
        
    }
    
    func notify(){
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .appStore_newState, object: nil, userInfo: nil)
        }
    }
    
    func unsubscribe<T>(_ view:T) {
        
        let ID = "\(T.self)_" + String(format: "%p", unsafeBitCast(T.self, to: Int.self))
        
        for item in _observerArray {
            if item.ID == ID {
                item.unsubscribe()
            }
        }
        _observerArray = _observerArray.filter { $0.ID != ID }
    }
    
    
    func apply(){
        Defaults[.app] = store.toJSON()
        notify()
    }
}
