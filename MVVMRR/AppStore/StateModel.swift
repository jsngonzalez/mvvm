//
//  StateModel.swift
//  pixo
//
//  Created by MacBook on 9/7/19.
//  Copyright © 2019 hidesoft. All rights reserved.
//

import ObjectMapper


enum stateType {
    case error_servicio
    case none
    case mostrar_cargando
    case ocultar_cargando
    case refrescar
}


struct StateModel {
    private var _type : stateType = .none
    private var _mensaje = ""
    
    var type : stateType {
        get{
            return _type
        }
    }
    
    var mensaje : String {
        get{
            return _mensaje
        }
    }
    
    init(_ type:stateType = .none, mensaje:String =  "") {
        _type = type
        _mensaje = mensaje
    }
}

struct errorString {
    static var error_internet = "No hay conexión a internet, por favor intentalo de nuevo."
}
