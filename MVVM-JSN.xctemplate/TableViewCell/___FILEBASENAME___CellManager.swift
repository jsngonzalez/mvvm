//
//  ___VARIABLE_moduleName___CellManager.swift
//  blackbold
//
//  Created by User on 1/30/21.
//

import Foundation
import UIKit

enum ___VARIABLE_moduleName___CellManager {
    // case cell(delegate: ___VARIABLE_moduleName___CellDelegate)
    case cell
}

extension ___VARIABLE_moduleName___CellManager: CellTypeProtocol {
    var height: CGFloat {
        return 110
    }

    var identifier: String {
        return ___VARIABLE_moduleName___Cell.cellIdentifier
    }
    
    func getCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        // switch self {
        // case .cell(let delegate):
        //     let cell = ___VARIABLE_moduleName___Cell.createCell(table: tableView, index: indexPath)
        //     cell.configure(delegate: delegate)
        //     return cell
        // }

        let cell = ___VARIABLE_moduleName___Cell.createCell(table: tableView, index: indexPath)
        cell.configure()
        return cell
        
    }
    
}
