//
//  ___VARIABLE_moduleName___CellProtocols.swift
//  blackbold
//
//  Created by User on 1/18/21.
//

import Foundation

protocol ___VARIABLE_moduleName___CellProtocol: AnyObject {
    
}

protocol ___VARIABLE_moduleName___CellViewModelProtocol: AnyObject {
    
}

protocol ___VARIABLE_moduleName___CellDelegate: AnyObject {
    
}