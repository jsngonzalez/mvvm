//
//  ___VARIABLE_moduleName___Cell.swift
//  blackbold
//
//  Created by User
//

import UIKit

class ___VARIABLE_moduleName___Cell: UICollectionViewCell {
    // weak var delegate: ___VARIABLE_moduleName___CellDelegate?
    var viewModel: ___VARIABLE_moduleName___CellViewModelProtocol?
    
    // delegate: ___VARIABLE_moduleName___CellDelegate
    func configure() {
        // self.delegate = delegate
        self.viewModel = ___VARIABLE_moduleName___CellViewModel(withView: self)
        setupUI()
        setup()
    }
    
    func setupUI() {
        
    }
    
    func setup() {
        
    }
}

extension ___VARIABLE_moduleName___Cell {
    static func createCell(table: UICollectionView, index: IndexPath) -> ___VARIABLE_moduleName___Cell {
        guard let cell = table.dequeueReusableCell(withReuseIdentifier: self.cellIdentifier,
                for: index) as? ___VARIABLE_moduleName___Cell else {
            return ___VARIABLE_moduleName___Cell()
        }
        return cell
    }
}
