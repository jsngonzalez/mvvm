//
//  ___VARIABLE_moduleName___ViewModel.swift
//  blackbold
//
//  Created by User
//

import Foundation

class ___VARIABLE_moduleName___CellViewModel: BaseViewModel {
    weak var view: ___VARIABLE_moduleName___CellProtocol?
    
    init(withView view: ___VARIABLE_moduleName___CellProtocol) {
        self.view = view
        let dependences = BaseDependencies([.storeManager])
        super.init(dependencies: dependences)
    }
}

extension ___VARIABLE_moduleName___CellViewModel: ___VARIABLE_moduleName___CellViewModelProtocol {

}
