//
//  ___VARIABLE_moduleName___Protocols.swift
//  blackbold
//
//  Created by User
//

import Foundation

protocol ___VARIABLE_moduleName___CellDelegate: AnyObject {
    
}

protocol ___VARIABLE_moduleName___CellProtocol: AnyObject {
    
}

protocol ___VARIABLE_moduleName___CellViewModelProtocol: AnyObject {
    
}