//
//  ___VARIABLE_moduleName___NetworkRouter.swift
//  blackbold
//
//  Created by User 
//

import Foundation

enum ___VARIABLE_moduleName___NetworkRouter {
    //case config
}

extension ___VARIABLE_moduleName___NetworkRouter: EndPointType {

    var path: String {
        return ""
        // switch self {
        // case .config:
        //     return ""
        // }
    }
    
    var data: Data? {
        return nil
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var parameters: Parameters? {
        return nil
    }
    
    var mockPath: URL? {
        return ""
        // switch self {
        // case .config:
        //     return R.file.configJson()?.absoluteURL
        // }
    }
    
}
