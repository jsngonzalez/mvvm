//
//  ___VARIABLE_moduleName___NetworkManager.swift
//  blackbold
//
//  Created by User 
//

import Foundation

protocol ___VARIABLE_moduleName___NetworkManagerProtocol {
    var shared: ___VARIABLE_moduleName___NetworkManagerProtocol { get }
    
    // func config(_ completion: @escaping (ResultService<ConfigModel, Error>) -> Void)
}

class ___VARIABLE_moduleName___NetworkManager: DefaulNetworkManager, ___VARIABLE_moduleName___NetworkManagerProtocol {
    var service: Services<___VARIABLE_moduleName___NetworkRouter>
    var shared: ___VARIABLE_moduleName___NetworkManagerProtocol {
        get {
            return ___VARIABLE_moduleName___NetworkManager(service)
        }
    }
    
    init(_ service: Services<___VARIABLE_moduleName___NetworkRouter> = Services<___VARIABLE_moduleName___NetworkRouter>()) {
        self.service = service
    }
    
    // func config(_ completion: @escaping (ResultService<ConfigModel, Error>) -> Void) {
    //     service.request(.config) { (data, response, error) in
    //         guard let response = response, error == nil else {
    //             completion(.failure(ErrorType.nilFound))
    //             return
    //         }
    //         let result = self.handleNetwork(response, data: data, as: ConfigModel.self)
    //         switch result {
    //         case .success(let data):
    //             completion(.success(data))
    //         case .failure(let error):
    //             completion(.failure(error))
    //         }
    //     }
    // }
    
}
