//
//  ___VARIABLE_moduleName___Protocols.swift
//
//  Created by User
//

import Foundation

protocol ___VARIABLE_moduleName___RouterProtocol: AnyObject {
    var view: ___VARIABLE_moduleName___ViewProtocol? { get set }
    var viewModel: ___VARIABLE_moduleName___ViewModelProtocol? { get set }
    //var delegate: ___VARIABLE_moduleName___Delegate? { get set }

}

protocol ___VARIABLE_moduleName___ViewModelProtocol: AnyObject {
    var view: ___VARIABLE_moduleName___ViewProtocol? { get set }
    var router: ___VARIABLE_moduleName___RouterProtocol? { get set }

    func setupListCell()
}

protocol ___VARIABLE_moduleName___ViewProtocol: BaseView {
    var viewModel: ___VARIABLE_moduleName___ViewModelProtocol? { get set }

    func reloadTable(withList list: [CollectionCellTypeProtocol])
}

protocol ___VARIABLE_moduleName___Delegate: AnyObject {
    
}
