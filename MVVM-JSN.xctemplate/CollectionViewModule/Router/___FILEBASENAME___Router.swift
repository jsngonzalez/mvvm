//
//  ___VARIABLE_moduleName___Router.swift
//
//  Created by User
//

import Foundation
import UIKit

class ___VARIABLE_moduleName___Router: BaseRouter {
    weak var view: ___VARIABLE_moduleName___ViewProtocol?
    var viewModel: ___VARIABLE_moduleName___ViewModelProtocol?
    //weak var delegate: ___VARIABLE_moduleName___Delegate?
    
    //delegate: ___VARIABLE_moduleName___Delegate
    static func createModule() -> ___VARIABLE_moduleName___ViewController {
        
        let ref = ___VARIABLE_moduleName___ViewController.instance(fromStoryboard: ___VARIABLE_moduleName___Config.storyboard)
        
        let router = ___VARIABLE_moduleName___Router()
        router.view = ref
        
        let viewModel: ___VARIABLE_moduleName___ViewModelProtocol = ___VARIABLE_moduleName___ViewModel()
        
        //router.delegate = delegate
        router.viewModel = viewModel
        viewModel.view = ref
        viewModel.router = router
        ref.viewModel = viewModel
        return ref
    }
}

extension ___VARIABLE_moduleName___Router: ___VARIABLE_moduleName___RouterProtocol {

}
