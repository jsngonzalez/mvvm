//
//  ___VARIABLE_moduleName___ViewMolde.swift
//
//  Created by User
//

import Foundation

class ___VARIABLE_moduleName___ViewModel: BaseViewModel {
    weak var view: ___VARIABLE_moduleName___ViewProtocol?
    var router: ___VARIABLE_moduleName___RouterProtocol?
    
    init() {
        super.init()
    }
}

extension ___VARIABLE_moduleName___ViewModel: ___VARIABLE_moduleName___ViewModelProtocol {
    
    func setupListCell() {
        var list: [CellTypeProtocol] = []
        self.view?.reloadTable(withList: list)
    }
}
