//
//  ___VARIABLE_moduleName___ViewController.swift
//
//  Created by User
//

import UIKit

class ___VARIABLE_moduleName___ViewController: BaseView {
    
    @IBOutlet var tableView: UITableView!
    
    var viewModel: ___VARIABLE_moduleName___ViewModelProtocol?

    let tableManager = TableViewManager()
    internal lazy var cellList = [CellTypeProtocol]() {
        didSet {
            self.tableManager.updateSource(cellList)
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        self.tableManager.setup(tableView: tableView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel?.setupListCell()
    }

}

extension ___VARIABLE_moduleName___ViewController: ___VARIABLE_moduleName___ViewProtocol {

    func reloadTable(withList list: [CellTypeProtocol]) {
        cellList = list
    }
}
