//
//  ___VARIABLE_moduleName___ViewController.swift
//
//  Created by User
//

import UIKit

class ___VARIABLE_moduleName___ViewController: BaseView {
    
    var viewModel: ___VARIABLE_moduleName___ViewModelProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

extension ___VARIABLE_moduleName___ViewController: ___VARIABLE_moduleName___ViewProtocol {

}
